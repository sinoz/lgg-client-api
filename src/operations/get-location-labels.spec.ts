import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("get-location-labels", t => withContext(async context => {
    await initializeMocks();

    const client = context.createApplicationClient({
        basic: { username: "MQ==", password: "dHJ1ZQ==" },
    });

    {
        const response = await client.getLocationLabel({
            parameters: {},
        });
        assert(response.status === 200);

        const responseEntity = await response.entity();

        t.deepEqual(
            responseEntity,
            {
                provider: [
                    "bla-123",
                    "wikipedia",
                ],
                location: [
                    "example",
                    "loc-z",
                    "loc-x",
                    "loc-y",
                ],
            },
        );
    }

    async function initializeMocks() {
        context.servers.auth.registerValidateClientSecretOperation(async incomingRequest => {
            const incomingEntity = await incomingRequest.entity();
            const clientId = incomingRequest.parameters.client;
            const clientSecret = incomingEntity.value;

            return {
                status: 200,
                parameters: {},
                entity() {
                    return { valid: clientId === "MQ==" && clientSecret === "dHJ1ZQ==" };
                },
            };
        });

        await context.services.pgPool.query(`
INSERT INTO public.location( id, created_utc )
VALUES( E'\\x01', '2012-12-01T12:00Z' ),
( E'\\x02', '2012-12-01T12:00Z' ),
( E'\\x03', '2012-12-01T12:00Z' ),
( E'\\x04', '2012-12-01T12:00Z' );

INSERT INTO public.location_label( location, name, value )
VALUES( E'\\x01', 'provider', 'wikipedia'),
( E'\\x01', 'location', 'example' ),
( E'\\x02', 'provider', 'bla-123' ),
( E'\\x02', 'location', 'loc-x' ),
( E'\\x03', 'provider', 'bla-123' ),
( E'\\x03', 'location', 'loc-y' ),
( E'\\x04', 'provider', 'bla-123' ),
( E'\\x04', 'location', 'loc-z' );
        
INSERT INTO public.beacon(
    id, version,
    ipv4, ipv6,
    client, location,
    created_utc, last_seen_utc
)
VALUES(
    '5457ef04-4702-4166-a996-b798474ee185', 'v0.1.0',
    '127.0.255.250', '2001:db8::8a2e:370:7334',
    E'\\x31', E'\\x01',
    '2012-12-01T12:00Z', '2012-12-01T12:00Z'
),
(
    '6457ef04-4702-4166-a996-b798474ee185', 'v0.1.0',
    '127.0.255.251', '2001:db8::8a2e:370:7335',
    E'\\x31', E'\\x02',
    '2012-12-01T12:00Z', '2012-12-01T12:00Z'
),
(
    '6457ef04-4702-4166-a996-b798474ee186', 'v0.1.0',
    '127.0.255.252', '2001:db8::8a2e:370:7336',
    E'\\x31', E'\\x03',
    '2012-12-01T12:00Z', '2012-12-01T12:00Z'
),
(
    '6457ef04-4702-4166-a996-b798474ee187', 'v0.1.0',
    '127.0.255.253', '2001:db8::8a2e:370:7337',
    E'\\x31', E'\\x04',
    '2012-12-01T12:00Z', '2012-12-01T12:00Z'
);
`);
    }
}));
