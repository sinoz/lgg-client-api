import * as clientApi from "@latency.gg/lgg-client-oas";
import { minute } from "msecs";
import tweetnacl from "tweetnacl-ts";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createLocationMetricsOperation(
    context: application.Context,
): clientApi.GetLocationMetricsOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const { client } = authorization.basic;
        const { providerName, locationName, probeIp } = incomingRequest.parameters;

        const now = new Date();

        const maxAge = new Date(
            now.valueOf() - 30 * minute,
        );

        const lastSeenMinimum = new Date(
            now.valueOf() - 10 * minute,
        );

        const queryResult = await withPgTransaction(
            context,
            "select-measurement",
            async pgClient => {
                const result = await pgClient.query(`
select beacon.ipv4, beacon.ipv6,
beacon.public_key,
sum(rtt_ms) as rtt_ms, sum(stddev) as stddev, count(rtt_ms) as count
from public.beacon
inner join public.location_label as p
    on beacon.location = p.location
    and p.name = 'provider'
inner join public.location_label as l
    on beacon.location = l.location
    and l.name = 'location'
left join public.sample_udp_data
    on beacon.id = sample_udp_data.beacon
    and sample_udp_data.source = $3
    and sample_udp_data.timestamp_utc > $4
where p.value = $1
and l.value = $2
and beacon.last_seen_utc > $5
group by 1, 2, 3
;
`,
                    [
                        providerName,
                        locationName,
                        probeIp,
                        maxAge.toISOString(),
                        lastSeenMinimum.toISOString(),
                    ],
                );
                return result;
            },
        );

        const beacons = new Array<clientApi.BeaconSchema>();
        let rttSum = 0;
        let stddevSum = 0;
        let measurementCount = 0;
        for (const { ipv4, ipv6, public_key, rtt_ms, stddev, count } of queryResult.rows) {
            let token: | string | undefined;
            if (public_key != null) {
                const encoder = new TextEncoder();
                const messageBuffer = encoder.encode(now.toISOString() + "/" + probeIp);

                const tokenBuffer = Buffer.from(
                    tweetnacl.sealedbox(messageBuffer, public_key),
                );
                token = tokenBuffer.toString("base64");
            }

            if (rtt_ms != null && stddev != null) {
                rttSum += parseFloat(rtt_ms);
                stddevSum += parseFloat(stddev);

                measurementCount += parseInt(count, 10);
            }

            beacons.push({
                ipv4: ipv4 ?? "", ipv6: ipv6 ?? "",
                token,
            });
        }

        let rtt;
        let stddev;
        if (measurementCount > 0) {
            rtt = Math.round(rttSum / measurementCount);
            stddev = Math.round(stddevSum / measurementCount);
        }
        else {
            rtt = 0;
            stddev = 0;
        }

        const entity: clientApi.LocationMetricsResponseSchema = {
            beacons,
            rtt,
            stddev,
            stale: measurementCount < 5,
        };

        return {
            status: 202,
            parameters: {},
            entity() {
                return entity;
            },
        };
    };
}
