import * as clientApi from "@latency.gg/lgg-client-oas";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createGetLocationLabelOperation(
    context: application.Context,
): clientApi.GetLocationLabelOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const queryResult = await withPgTransaction(
            context,
            "select-location-label",
            async pgClient => {
                const result = await pgClient.query(`
select name, value
from public.location_label
group by 1, 2
;
`,
                );
                return result;
            },
        );

        const entity = queryResult.rows.reduce<Record<string, string[]>>(
            (entity, row) => {
                if (entity[row.name] == null) {
                    entity[row.name] = [row.value];
                }
                else {
                    entity[row.name].push(row.value);
                }
                return entity;
            },
            {},
        );

        return {
            status: 200,
            parameters: {},
            entity() { return entity; },
        };
    };
}
