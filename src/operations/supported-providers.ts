import * as clientApi from "@latency.gg/lgg-client-oas";
import { minute } from "msecs";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createGetSupportedProvidersOperation(
    context: application.Context,
): clientApi.GetSupportedProvidersOperationHandler<application.ServerAuthorization> {
    return async (
        incomingRequest,
        authorization,
    ) => {
        const { client } = authorization.basic;

        const now = new Date();
        const lastSeenMinimum = new Date(
            now.valueOf() - 1 * minute,
        );

        const queryResult = await withPgTransaction(
            context,
            "select-providers",
            async pgClient => {
                const result = await pgClient.query(`
select distinct p.value as provider, l.value as location
from public.beacon
inner join public.location_label as l
on l.location = beacon.location
inner join public.location_label as p
on p.location = beacon.location
where l.name = 'location'
and p.name = 'provider'
and beacon.last_seen_utc > $1
order by 1
;
`,
                    [
                        lastSeenMinimum.toISOString(),
                    ],
                );
                return result;
            },
        );

        const providers = new Array<clientApi.ProviderSchema>();
        {
            let providerPrev: string | undefined;
            let providerEntity: clientApi.ProviderSchema | undefined;
            for (const { provider, location } of queryResult.rows) {
                if (providerEntity && providerPrev !== provider) {
                    providers.push(providerEntity);
                    providerEntity = undefined;
                }

                if (!providerEntity) {
                    providerEntity = {
                        name: provider,
                        locations: new Array<string>(),
                    };
                }

                providerEntity.locations.push(location);

                providerPrev = provider;
            }
            if (providerEntity) {
                providers.push(providerEntity);
            }
        }

        return {
            status: 202,
            parameters: {},
            entity() {
                return providers;
            },
        };
    };
}
