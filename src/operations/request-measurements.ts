import * as clientApi from "@latency.gg/lgg-client-oas";
import { hour, minute } from "msecs";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createRequestMeasurementsOperation(
    context: application.Context,
): clientApi.RequestMeasurementsOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const now = new Date();
        const since = new Date(now.valueOf() - 12 * hour);
        const lastSeenMinimum = new Date(now.valueOf() - 1 * minute);

        const source = incomingRequest.parameters.source;
        const types = incomingRequest.parameters.type;
        const locations = incomingRequest.parameters.location;
        const locationBuffers = locations.map(location => Buffer.from(location, "base64"));

        const targets: {
            "udp-data"?: Record<string, string[]>,
        } = {};

        for (const type of types) {
            switch (type) {
                case "udp-data": {
                    const queryResult = await withPgTransaction(
                        context,
                        "stale-targets-udp-data",
                        async pgClient => {
                            const result = await pgClient.query(`
select beacon.location, beacon.ipv4, beacon.ipv6
from public.beacon
where beacon.location = any($3)
and beacon.last_seen_utc > $4
and beacon.location not in (
    select beacon.location
    from public.measurement
    inner join public.sample_udp_data
    on measurement.timestamp_utc = sample_udp_data.timestamp_utc
    and measurement.source = sample_udp_data.source
    inner join public.beacon
    on sample_udp_data.beacon = beacon.id
    where measurement.timestamp_utc > $1
    and measurement.source = $2
    and beacon.location = any($3)
)
;
`,
                                [
                                    since.toISOString(),
                                    source,
                                    locationBuffers,
                                    lastSeenMinimum.toISOString(),
                                ],
                            );
                            return result;
                        },
                    );
                    if (queryResult.rowCount > 0) {
                        queryResult.rows.sort(() => Math.random() > 0.5 ? 1 : -1);

                        targets["udp-data"] = queryResult.rows.
                            map(
                                row => [row.location.toString("base64"), row.ipv4, row.ipv6],
                            ).
                            reduce<Array<[string, string]>>(
                                (list, [location, ipv4, ipv6]) => {
                                    if (ipv4 != null) list.push([location, ipv4]);
                                    if (ipv6 != null) list.push([location, ipv6]);
                                    return list;
                                },
                                [],
                            ).
                            reduce<Record<string, string[]>>(
                                (map, [location, target]) => {
                                    if (location in map) {
                                        map[location].push(target);
                                    }
                                    else {
                                        map[location] = [target];
                                    }
                                    return map;
                                },
                                {},
                            );
                    }
                    break;
                }
            }
        }

        if (Object.keys(targets).length === 0) {
            return {
                status: 304,
                parameters: {},
            };
        }

        const clientIdBuffer = authorization.basic.client;

        await withPgTransaction(
            context,
            "insert measurement",
            async pgClient => {
                const result = await pgClient.query(`
insert into public.measurement (
    timestamp_utc, source, client
)
values (
    $1, $2, $3
)
;
`,
                    [
                        now.toISOString(),
                        source,
                        clientIdBuffer,
                    ],
                );
                return result;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    timestamp: now.valueOf(),
                    targets,
                };
            },
        };
    };
}
