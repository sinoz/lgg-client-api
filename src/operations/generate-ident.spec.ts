import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("generate ident", t => withContext(async context => {
    initializeMocks();

    const client = context.createApplicationClient({
        basic: { username: "MQ==", password: "dHJ1ZQ==" },
    });

    const response = await client.generateIdent({
        parameters: {
            probeIp: "2001:db8::2021:6a4f:4af",
        },
    });
    assert(response.status === 202);

    const responseEntity = await response.entity();
    const identBuffer = Buffer.from(responseEntity.ident, "base64");
    const pubBuffer = Buffer.from(responseEntity.pub, "base64");

    t.equal(responseEntity.ident.length, 88);
    t.equal(identBuffer.length, 64);
    t.equal(pubBuffer.length, 32);

    function initializeMocks() {
        context.servers.auth.registerValidateClientSecretOperation(async incomingRequest => {
            const incomingEntity = await incomingRequest.entity();
            const clientId = incomingRequest.parameters.client;
            const clientSecret = incomingEntity.value;

            return {
                status: 200,
                parameters: {},
                entity() {
                    return { valid: clientId === "MQ==" && clientSecret === "dHJ1ZQ==" };
                },
            };
        });
    }
}));
