import * as clientApi from "@latency.gg/lgg-client-oas";
import tweetnacl from "tweetnacl-ts";
import * as application from "../application/index.js";

const privateKey = Buffer.from("ukrVY6t5NpnyC6cmMRKPtfCqYYUbVY0OFAHAg8vyaP2hOw2JuHns2R7NUD7+WNpc9cAlHjJGlJ1YyI88r7wBoA==", "base64");

export function createGenerateIdentOperation(
    context: application.Context,
): clientApi.GenerateIdentOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const { client } = authorization.basic;
        const { probeIp } = incomingRequest.parameters;

        const encoder = new TextEncoder();
        const message = encoder.encode(probeIp);

        const keyPair = tweetnacl.sign_keyPair_fromSecretKey(privateKey);
        const publicKeyBuffer = Buffer.from(keyPair.publicKey);

        const signatureBuffer = Buffer.from(tweetnacl.sign_detached(message, keyPair.secretKey));

        const ident = signatureBuffer.toString("base64");
        const pub = publicKeyBuffer.toString("base64");

        return {
            status: 202,
            parameters: {},
            entity() {
                return {
                    ident,
                    pub,
                };
            },
        };

    };
}
