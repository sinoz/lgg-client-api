import * as clientApi from "@latency.gg/lgg-client-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import * as authorization from "../authorization/index.js";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type ServerAuthorization = authorization.BasicAuthorization;

export type Server = clientApi.Server<ServerAuthorization>;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new clientApi.Server<ServerAuthorization>({
        baseUrl: context.config.endpoint,
    });

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(clientApi.metadata, route.name, request.method) :
                undefined;
            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    server.registerBasicAuthorization(
        authorization.createBasicAuthorization(context),
    );

    server.registerGenerateIdentOperation(
        operations.createGenerateIdentOperation(context),
    );

    server.registerGetLocationMetricsOperation(
        operations.createLocationMetricsOperation(context),
    );

    server.registerGetSupportedProvidersOperation(
        operations.createGetSupportedProvidersOperation(context),
    );

    server.registerGetSupportedProviderLocationsOperation(
        operations.createGetSupportedProviderLocationsOperation(context),
    );

    server.registerGetMetricsOperation(
        operations.createGetMetricsOperation(context),
    );

    server.registerGetLocationLabelOperation(
        operations.createGetLocationLabelOperation(context),
    );

    server.registerRequestMeasurementsOperation(
        operations.createRequestMeasurementsOperation(context),
    );

    server.registerGetMetricsUdpDataOperation(
        operations.createGetMetricsUdpDataOperation(context),
    );

    server.registerRequestMeasurementUdpDataOperation(
        operations.createRequestMeasurementUdpDataOperation(context),
    );

    return server;
}
