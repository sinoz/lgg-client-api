import test from "tape-promise/tape.js";
import { withContext } from "./context.js";

test("with-context", t => withContext(async context => {
    t.pass();

    const result = await context.services.pgPool.query("SELECT 1");

    t.equal(result.rowCount, 1);
}));
